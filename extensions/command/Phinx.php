<?php

namespace li3_phinx\extensions\command;

use \lithium\data\Connections;
use \lithium\core\Environment;

/**
 * Acts as a wrapper for phinx.
 */
class Phinx extends \lithium\console\Command {

  public $connection = 'default';
  public $environment = 'app';
  public $configuration;
  public $bin;

  public function run($command = null) {
    $env = Environment::get();
    $args = $this->_config['request']->args;
    $this->configuration = $this->configuration ?: ('app/config/db/phinx.php');
    $this->bin = $this->bin ?: (LITHIUM_LIBRARY_PATH . '/robmorgan/phinx/bin/phinx');
    $this->execute($command, $args);
  }

  public function execute($command, $args) {
    $argv = array_merge(array($command), $args);
    if( in_array($command, ['migrate', 'rollback', 'status']) )
      $shell_cmd = "PHINX_DB_ENV={$this->environment} php {$this->bin} --configuration=\"{$this->configuration}\" --environment={$this->environment} " . implode(' ', $argv);
    else
      $shell_cmd = "PHINX_DB_ENV={$this->environment} php {$this->bin} --configuration=\"{$this->configuration}\" " . implode(' ', $argv);
    passthru($shell_cmd);
  }

}

