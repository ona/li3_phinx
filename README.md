## Introduction
**li3_phinx** is a [li3][0] command plugin that acts as a simple wrapper for phinx by making the commands available as part of the li3 command set.

## Installation/Configuration
1. ``composer require ona/li3_phinx``

2. Add the plugin in your ``libraries.php (/path/to/li3_app/config/bootstrap/libraries.php)``:

        Libraries::add('li3_phinx');

##Usage
        
        /path/to/li3_app$ ./li3 phinx [command]

**Note** refer to the [original phinx documentation][2] for detailed usage instructions.


[0]:http://li3.me/
[1]:https://github.com/robmorgan/phinx
[2]:http://docs.phinx.org/
